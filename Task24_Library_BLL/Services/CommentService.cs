﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task24_Library_BLL.DTO;
using Task24_Library_BLL.ServiceInterfaces;
using Task24_Library_BLL.Validation;
using Task24_Library_DALL.Entities;
using Task24_Library_DALL.UoW;

namespace Task24_Library_BLL.Services
{
    public class CommentService : ICommentService
    {
        IUnitOfWork Database { get; set; }

        public CommentService(IUnitOfWork iow)
        {
            Database = iow;
        }

        /// <summary>
        /// Add comment in database
        /// </summary>
        /// <param name="commentDTO"></param>
        public void AddNewComment(CommentDTO commentDTO)
        {
            if (commentDTO == null)
                throw new ArgumentNullException(nameof(commentDTO), "commentDTO equal null");

            var mapper = new MapperConfiguration(conf => conf.CreateMap<CommentDTO, Comment>()).CreateMapper();

            Database.Comment.Add(mapper.Map<CommentDTO, Comment>(commentDTO));
            Database.Save();
        }

        public IEnumerable<CommentDTO> GetAllComments()
        {
            var comments = Database.Comment.GetAll();

            if (comments == null)
                throw new ValidationException("Comments not found in Database!");

            var mapper = new MapperConfiguration(conf => conf.CreateMap<Comment, CommentDTO>()).CreateMapper();

            return mapper.Map<IEnumerable<Comment>, List<CommentDTO>>(comments);
        }

        public void Dispose() => Database.Dispose();
    }
}
