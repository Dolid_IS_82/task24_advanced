﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task24_Library_BLL.DTO;
using Task24_Library_BLL.ServiceInterfaces;
using Task24_Library_BLL.Validation;
using Task24_Library_DALL.Entities;
using Task24_Library_DALL.UoW;

namespace Task24_Library_BLL.Services
{
    public class NewsService : INewsService
    {
        IUnitOfWork Database { get; set; }

        public NewsService(IUnitOfWork iow)
        {
            Database = iow;
        }

        /// <summary>
        /// Method returns all news
        /// </summary>
        /// <returns>List of all news</returns>
        public IEnumerable<NewsDTO> GetAllNews()
        {
            var news = Database.News.GetAll();

            if(news == null)
                throw new ValidationException("News not found in Database!");

            var mapper = new MapperConfiguration(conf => conf.CreateMap<News, NewsDTO>()).CreateMapper();

            return mapper.Map<IEnumerable<News>, List<NewsDTO>>(news);
        }

        public void Dispose() => Database.Dispose();
    }
}
