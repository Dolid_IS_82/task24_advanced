﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task24_Library_BLL.DTO;

namespace Task24_Library_BLL.ServiceInterfaces
{
    public interface ICommentService
    {
        /// <summary>
        /// Method returns all comments
        /// </summary>
        /// <returns>List of all comments</returns>
        IEnumerable<CommentDTO> GetAllComments();

        /// <summary>
        /// Add comment in database
        /// </summary>
        /// <param name="commentDTO"></param>
        void AddNewComment(CommentDTO commentDTO);

        /// <summary>
        /// Dispose method
        /// </summary>
        void Dispose();
    }
}
