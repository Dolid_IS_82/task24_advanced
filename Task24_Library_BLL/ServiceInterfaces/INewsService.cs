﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task24_Library_BLL.DTO;

namespace Task24_Library_BLL.ServiceInterfaces
{
    public interface INewsService
    {
        /// <summary>
        /// Method returns all news
        /// </summary>
        /// <returns>List of all news</returns>
        IEnumerable<NewsDTO> GetAllNews();

        /// <summary>
        /// Dispose method
        /// </summary>
        void Dispose();
    }
}
