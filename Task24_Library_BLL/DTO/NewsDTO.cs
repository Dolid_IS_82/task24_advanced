﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task24_Library_BLL.DTO
{
    public class NewsDTO
    {
        public int Id { get; set; }
        public string Topic { get; set; }
        public DateTime DateOfNews { get; set; }
        public string Content { get; set; }
    }
}
