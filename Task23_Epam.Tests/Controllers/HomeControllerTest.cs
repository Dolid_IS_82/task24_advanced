﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Task23_Epam;
using Task23_Epam.Controllers;
using Task24_Library_BLL.DTO;
using Task24_Library_BLL.ServiceInterfaces;

namespace Task23_Epam.Tests.Controllers
{
    [TestFixture]
    public class HomeControllerTest
    {
        private List<NewsDTO> articles;

        [SetUp]
        public void Setup()
        {
            articles = new List<NewsDTO>
            {
                new NewsDTO
                {
                    Topic = "Business",
                    DateOfNews = new DateTime(2017, 12, 25),
                    Content = "Some text about that...."
                },
                new NewsDTO
                {
                    Topic = "Sport",
                    DateOfNews = new DateTime(2020, 10, 10),
                    Content = "Some text about that...."
                },
                new NewsDTO
                {
                    Topic = "Education",
                    DateOfNews = new DateTime(2020, 12, 12),
                    Content = "Some text about that...."
                },
                new NewsDTO
                {
                    Topic = "Guitar",
                    DateOfNews = new DateTime(2021, 2, 13),
                    Content = "Some text about that...."
                },
            };
        }


        [Test]
        public void StartGetIndex_ExpectNotNullResult()
        {
            // Arrange
            var newsService = new Mock<INewsService>();
            newsService.Setup(x => x.GetAllNews()).Returns(articles);

            HomeController controller = new HomeController(newsService.Object);

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

    }
}
