﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Task23_Epam;
using Task23_Epam.Controllers;
using Task23_Epam.Models;
using Task24_Library_BLL.DTO;
using Task24_Library_BLL.ServiceInterfaces;

namespace Task23_Epam.Tests.Controllers
{
    [TestFixture]
    public class GuestControllerTest
    {
        List<CommentDTO> comments;
        
        [SetUp]
        public void Setup()
        {
            comments = new List<CommentDTO>
                {
                    new CommentDTO
                    {
                        Name = "Chris Fox",
                        Text = "Good site. 8/10 ",
                        CommentDate = new DateTime(2017, 12, 25)
                    },
                    new CommentDTO
                    {
                        Name = "Rebecca Flex",
                        Text = "I want to work in Epam",
                        CommentDate = new DateTime(2020, 10, 10)
                    },
                    new CommentDTO
                    {
                        Name = "Vasya Pupkin",
                        Text = "Year! Me too!",
                        CommentDate = new DateTime(2020, 12, 12)
                    },
                    new CommentDTO
                    {
                        Name = "Vasya Pupkin",
                        Text = "I personally know the developer",
                        CommentDate = new DateTime(2021, 2, 13)
                    }
                };
        }


        [Test]
        public void StartHttpGetGuest_ExpectNotNullResult()
        {
            // Arrange
            var commentService = new Mock<ICommentService>();

            commentService.Setup(x => x.GetAllComments()).Returns(comments);

            GuestController controller = new GuestController(commentService.Object);

            // Act
            ViewResult result = controller.Guest() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [Test]
        public void PostGuest_AddingCommentIntoBase_ExpectOneAdditionalElementsInBase()
        {
            // Arrange
            var commentService = new MockCommentService(comments);

            GuestController controller = new GuestController(commentService);
            var comment = new CommentModel("Vova", "Volodya");

            var expected = commentService.testComments.Count + 1;

            // Act
            ViewResult result = controller.Guest(comment) as ViewResult;
            var actual = commentService.testComments.Count;
            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void PostGuest_AddingNullCommentIntoBase_ReturnEmptyModelTheNumberOfElementsInBaseDoesntChange()
        {
            // Arrange
            var commentService = new MockCommentService(comments);

            GuestController controller = new GuestController(commentService);

            var expected = commentService.testComments.Count;

            // Act
            ViewResult result = controller.Guest(null) as ViewResult;
            var actual = commentService.testComments.Count;
            // Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
