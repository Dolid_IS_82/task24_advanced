﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Task23_Epam;
using Task23_Epam.Controllers;

namespace Task23_Epam.Tests.Controllers
{
    [TestClass]
    public class QuestionnaireControllerTest
    {
        [TestMethod]
        public void StartHttpGetQuestionnaire_ExpectNotNullResult()
        {
            // Arrange
            QuestionnaireController controller = new QuestionnaireController();

            // Act
            ViewResult result = controller.Questionnaire() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void PostQuestionnaire_SendResult_ExpectNotEmptyResult()
        {
            // Arrange
            QuestionnaireController controller = new QuestionnaireController();

            // Act
            string result = controller.Questionnaire("Something", "years", "Vova", "D", new List<string>(), new List<string>());

            // Assert
            Assert.AreNotEqual(result, String.Empty);
        }

        [TestMethod]
        public void PostQuestionnaire_SendNullData_ExpectNotNullResult()
        {
            // Arrange
            QuestionnaireController controller = new QuestionnaireController();

            // Act
            string result = controller.Questionnaire("Something", null, "Vova", "D", null, null);

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
