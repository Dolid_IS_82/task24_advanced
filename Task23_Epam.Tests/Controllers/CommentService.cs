﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task24_Library_BLL.DTO;
using Task24_Library_BLL.ServiceInterfaces;

namespace Task23_Epam.Tests.Controllers
{
    public class MockCommentService : ICommentService
    {
        public List<CommentDTO> testComments;
        public MockCommentService(List<CommentDTO> comments)
        {
            testComments = comments;
        }

        public void AddNewComment(CommentDTO commentDTO)
        {
            testComments.Add(commentDTO);
        }

        public IEnumerable<CommentDTO> GetAllComments()
        {
            return testComments;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }


    }
}
