﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Task24_Library_BLL.ServiceInterfaces;
using Task24_Library_BLL.Services;

namespace Task23_Epam.MySettingsDIandMap
{
    public class OrderModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ICommentService>().To<CommentService>();
            Bind<INewsService>().To<NewsService>();
        }
    }
}