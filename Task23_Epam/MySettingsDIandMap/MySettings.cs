﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using Task23_Epam.Models;
using Task24_Library_BLL.DTO;
using Task24_Library_BLL.ServiceInterfaces;
using Task24_Library_BLL.Services;
using Task24_Library_DALL.UoW;

namespace Task23_Epam.MySettingsDIandMap
{
    /// <summary>
    /// Settings class. Contains the creation of services and data conversion  
    /// </summary>
    public static class MySettings
    {
        /// <summary>
        /// Converts a CommentModel list to a CommentDTO list 
        /// </summary>
        /// <param name="commentModel"></param>
        /// <returns>Returns element ConvertDTO</returns>
        public static CommentDTO MapCommentModelToCommentDTO(CommentModel commentModel)
        {
            var mapper = new MapperConfiguration(conf => conf.CreateMap<CommentModel, CommentDTO>()).CreateMapper();

            return mapper.Map<CommentModel, CommentDTO>(commentModel);
        }


        /// <summary>
        /// Converts a NewsDTO list to a NewsModel list 
        /// </summary>
        /// <param name="newsDTOs"></param>
        /// <returns>List of NewsModel</returns>
        public static IEnumerable<NewsModel> MapListNewsDTOToNewsModel(IEnumerable<NewsDTO> newsDTOs)
        {
            var mapper = new MapperConfiguration(conf => conf.CreateMap<NewsDTO, NewsModel>()).CreateMapper();

            return mapper.Map<IEnumerable<NewsDTO>, IEnumerable<NewsModel>>(newsDTOs);
        }

        /// <summary>
        /// Converts a CommentDTO list to a CommentModel list 
        /// </summary>
        /// <param name="newsDTOs"></param>
        /// <returns>List of NewsModel</returns>
        public static IEnumerable<CommentModel> MapListCommentDTOToCommentModel(IEnumerable<CommentDTO> commentDTOs)
        {
            var mapper = new MapperConfiguration(conf => conf.CreateMap<CommentDTO, CommentModel>()).CreateMapper();

            return mapper.Map<IEnumerable<CommentDTO>, IEnumerable<CommentModel>>(commentDTOs);
        }
    }
}