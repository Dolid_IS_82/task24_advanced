﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Task23_Epam.Models
{
    public class NewsModel
    {
        public string Topic { get; set; }
        public DateTime DateOfNews { get; set; }
        public string Content { get; set; }
    }
}