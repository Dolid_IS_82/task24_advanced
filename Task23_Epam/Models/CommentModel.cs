﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Task23_Epam.Models
{
    public class CommentModel
    {
        [Required(ErrorMessage = "You must enter a name")]
        [StringLength(20, MinimumLength = 3)]
        public string Name { get; /*private*/ set; }
        public DateTime CommentDate { get; /*private*/ set; } = DateTime.Now;
        [Required(ErrorMessage = "You must enter some comment")]
        public string Text { get; /*private*/ set; }

        public CommentModel(string name, string text)
        {
            Name = name;
            Text = text;
            CommentDate = DateTime.Now; 
        }

        public CommentModel() { }
    }
}