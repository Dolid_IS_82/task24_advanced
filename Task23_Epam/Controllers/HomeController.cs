﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Task23_Epam.MySettingsDIandMap;
using Task24_Library_BLL.ServiceInterfaces;

namespace Task23_Epam.Controllers
{
    public class HomeController : Controller
    {
        // The service is public for writing tests, as DI is not allowed
        public readonly INewsService newsService;
        public HomeController(INewsService service)
        {
            newsService = service;
        }


        public ActionResult Index()
        {
            var articlesDTO = newsService.GetAllNews();

            if (articlesDTO == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            var articlesModel = MySettings.MapListNewsDTOToNewsModel(articlesDTO);

            if (articlesModel != null)
                return View(articlesModel);

            return new HttpStatusCodeResult(HttpStatusCode.NotFound);
        }

    }
}