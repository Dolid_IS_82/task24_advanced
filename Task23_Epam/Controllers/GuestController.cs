﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task23_Epam.Models;
using Task23_Epam.MySettingsDIandMap;
using Task24_Library_BLL.ServiceInterfaces;

namespace Task23_Epam.Controllers
{
    public class GuestController : Controller
    {
        private readonly ICommentService commentService;
        public GuestController(ICommentService service)
        {
            commentService = service;
        }

        // GET: Guest
        [HttpGet]
        public ActionResult Guest()
        {
            var commentsDTO = commentService.GetAllComments();
            var commentsModel = MySettings.MapListCommentDTOToCommentModel(commentsDTO);
            ViewBag.Comments = commentsModel;
            return View();
        }

        // POST: Guest
        [HttpPost]
        public ActionResult Guest(CommentModel comment)
        {
            if (ModelState.IsValid)
            {
                var commentModel = MySettings.MapCommentModelToCommentDTO(comment);

                if(commentModel == null)
                    return View();

                commentService.AddNewComment(commentModel);

                return RedirectToAction("Guest");
            }

            var commentsDTO = commentService.GetAllComments();
            var commentsModel = MySettings.MapListCommentDTOToCommentModel(commentsDTO);
            ViewBag.Comments = commentsModel;
            return View(comment);

        }
    }
}