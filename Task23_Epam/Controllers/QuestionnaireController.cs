﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Task23_Epam.Controllers
{
    public class QuestionnaireController : Controller
    {
        // GET: Questionnaire
        [HttpGet]
        public ActionResult Questionnaire()
        {
            return View();
        }

        [HttpPost]
        public string Questionnaire(string gender, string years, string name, string surname, List<string> service, List<string> ser)
        {
            string result = $"<h2>Result</h2>Your gender: {gender} <br />Your age: {years}<br />Your name: {name} <br />Your surname: {surname}<br />";
            result += "You find out about service from: ";
            for (int i = 0; i < service?.Count; i++)
            {
                result += service[i] + ", ";
            }

            result += "<br />Services that interest you: ";
            for (int i = 0; i < ser?.Count; i++)
            {
                result += ser[i] + ", ";
            }

            return result;
        }
    }
}