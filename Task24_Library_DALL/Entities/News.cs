﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task24_Library_DALL.Entities
{
    public class News
    {
        public int Id { get; set; }
        [Required]
        public string Topic { get; set; }
        public DateTime DateOfNews { get; set; }
        [Required]
        public string Content { get; set; }
    }
}
