﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task24_Library_DALL.Entities
{
    public class Comment
    {
        public int Id { get; set; }
        [Required, MinLength(3), MaxLength(20)]
        public string Name { get; set; }
        public DateTime CommentDate { get; set; }
        [Required]
        public string Text { get; set; }
    }
}
