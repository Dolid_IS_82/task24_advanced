﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task24_Library_DALL.Entities;

namespace Task24_Library_DALL.InterfaceRepositories
{
    public interface ICommentRepository : IRepository<Comment>
    {
    }
}
