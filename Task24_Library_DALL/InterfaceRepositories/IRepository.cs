﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Task24_Library_DALL.InterfaceRepositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        // Finding object
        TEntity Get(int id);
        IEnumerable<TEntity> GetAll();

        // Sorts data by the entered parameter 
        IEnumerable<TEntity> GetOrderedBy(Expression<Func<TEntity, bool>> condition);

        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);
        TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate);

        // Adding object
        void Add(TEntity entity);
        void AddRange(IEnumerable<TEntity> entities);

        // Removing object
        void Remove(TEntity entity);
        void RemoveRange(IEnumerable<TEntity> entities);

        // Update
        void Update(TEntity entity);
    }
}
