﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task24_Library_DALL.DBSettings;
using Task24_Library_DALL.Entities;
using Task24_Library_DALL.InterfaceRepositories;

namespace Task24_Library_DALL.Repositories
{
    public class CommentRepository : Repository<Comment>, ICommentRepository
    {
        public CommentRepository(LibraryDBContext myDbContext)
            : base(myDbContext)
        {
        }
    }
}
