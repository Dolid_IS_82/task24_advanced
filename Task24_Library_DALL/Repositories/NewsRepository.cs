﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task24_Library_DALL.DBSettings;
using Task24_Library_DALL.Entities;
using Task24_Library_DALL.InterfaceRepositories;

namespace Task24_Library_DALL.Repositories
{
    public class NewsRepository : Repository<News>, INewsRepository
    {
        public NewsRepository(LibraryDBContext myDbContext)
            : base(myDbContext)
        {
        }
    }
}
