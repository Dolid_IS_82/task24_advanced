﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task24_Library_DALL.DBSettings;
using Task24_Library_DALL.InterfaceRepositories;
using Task24_Library_DALL.Repositories;

namespace Task24_Library_DALL.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly LibraryDBContext _context;

        /// <summary>
        /// Creates contexts and repositories 
        /// </summary>
        /// <param name="connectionString"></param>
        public UnitOfWork(string connectionString)
        {
            _context = new LibraryDBContext(connectionString);
            Comment = new CommentRepository(_context);
            News = new NewsRepository(_context);
        }

        public ICommentRepository Comment { get; private set; }

        public INewsRepository News { get; private set; }

        public void Dispose()
        {
            _context.Dispose();
        }

        /// <summary>
        /// Save changes to the database  
        /// </summary>
        /// <returns></returns>
        public int Save()
        {
            return _context.SaveChanges();
        }
    }
}
