﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task24_Library_DALL.InterfaceRepositories;

namespace Task24_Library_DALL.UoW
{
    public interface IUnitOfWork : IDisposable
    {
        ICommentRepository Comment { get; }
        INewsRepository News { get; }

        int Save();
    }
}
