﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task24_Library_DALL.Entities;

namespace Task24_Library_DALL.DBSettings
{
    public class LibraryDBContext: DbContext
    {
        static LibraryDBContext()
        {
            Database.SetInitializer(new Initializer());
        }

        public LibraryDBContext(string connectionString) : base(connectionString)
        {
        }

        public LibraryDBContext() : base("DbConnectionString")
        { }

        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<News> News { get; set; }
    }
}
