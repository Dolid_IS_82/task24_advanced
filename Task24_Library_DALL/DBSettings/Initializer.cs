﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task24_Library_DALL.Entities;

namespace Task24_Library_DALL.DBSettings
{
    internal sealed class Initializer : CreateDatabaseIfNotExists<LibraryDBContext>
    {
        protected override void Seed(LibraryDBContext context)
        {
            base.Seed(context);

            List<Comment> comments = new List<Comment>
                {
                    new Comment
                    {
                        Name = "Chris Fox",
                        Text = "Good site. 8/10 ",
                        CommentDate = new DateTime(2017, 12, 25)
                    },
                    new Comment
                    {
                        Name = "Rebecca Flex",
                        Text = "I want to work in Epam",
                        CommentDate = new DateTime(2020, 10, 10)
                    },
                    new Comment
                    {
                        Name = "Vasya Pupkin",
                        Text = "Year! Me too!",
                        CommentDate = new DateTime(2020, 12, 12)
                    },
                    new Comment
                    {
                        Name = "Vasya Pupkin",
                        Text = "I personally know the developer",
                        CommentDate = new DateTime(2021, 2, 13)
                    }   
                };

            context.Comments.AddRange(comments);


            List<News> articles = new List<News>
            {
                new News
                {
                    Topic = "Business",
                    DateOfNews = new DateTime(2017, 12, 25),
                    Content = "Some text about that...."
                },
                new News
                {
                    Topic = "Sport",
                    DateOfNews = new DateTime(2020, 10, 10),
                    Content = "Some text about that...."
                },
                new News
                {
                    Topic = "Education",
                    DateOfNews = new DateTime(2020, 12, 12),
                    Content = "Some text about that...."
                },
                new News
                {
                    Topic = "Guitar",
                    DateOfNews = new DateTime(2021, 2, 13),
                    Content = "Some text about that...."
                },
            };
            context.News.AddRange(articles);

            context.SaveChanges();
        }
    }
}
